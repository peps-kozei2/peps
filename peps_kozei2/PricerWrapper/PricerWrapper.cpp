// Il s'agit du fichier DLL principal.
#include "stdafx.h"
#include "PricerWrapper.h"

using namespace pricer;
namespace PricerWrapper {
	/*
	void WrapperClass::create(int size, double r, double* rho, double*sigma, double*spot, double*trend, double h, int samples, double T, int TimeSteps, char*optionType, double InvestissementInit) {
		monteCarloInstance(size, r, rho, sigma, spot, trend, h, samples, T, TimeSteps, optionType, InvestissementInit)
	}
	*/
	WrapperClass::WrapperClass() {
		this->price = 0;
		this->confidenceInterval = 0;
	};

	double WrapperClass::getPrice() {
		return this->price;
	}

	double WrapperClass::getConfidence() {
		return this->confidenceInterval;
	}

	void WrapperClass::getPrice_0() {
		int size = 30;
		double r = 0.05;
		double* rho = new double(size*size);
		double* sigma = new double(size);
		double* spot = new double(size);
		double* trend = new double(size);
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++)
				rho[i*size + j] = (i == j) ? 1 : 0.2;
			sigma[i] = 0.2;
			spot[i] = 100;
			trend[i] = 0.07;
		}
		double h = 0.1;
		int samples = 100; 
		double T = 8.0;
		int TimeSteps = 16;
		char*optionType = "structured_product";
		double InvestissementInit = 100;

		monteCarloInstance(size, r, rho, sigma, spot, trend, h, samples, T, TimeSteps, optionType, InvestissementInit);
		double px=0, ic=0;
		price_0(px, ic);

		this->price = px;
		this->confidenceInterval = ic;

		monteCarloDestruct();
		delete rho;
		delete sigma;
		delete spot;
		delete trend;
	}
}