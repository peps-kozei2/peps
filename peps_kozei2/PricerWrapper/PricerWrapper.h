#pragma once
#include "pricer.hpp"

using namespace System;

namespace PricerWrapper {

	public ref class WrapperClass
	{
	private :
		double price;
		double confidenceInterval;
	public:
		WrapperClass();
		double getPrice();
		double getConfidence();
		
		void getPrice_0();
	};
}
