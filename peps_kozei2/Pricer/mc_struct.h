class MC_STRUCT
{
public:
	int size;
	double r; 
	double* rho; 
	double*sigma; 
	double*spot; 
	double*trend;
	double h;
	int samples;
	double T;
	int TimeSteps;
	char*optionType;
	double InvestissementInit;
}