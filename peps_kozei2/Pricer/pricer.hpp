#pragma once


#define DLLEXP   __declspec( dllexport )

namespace pricer{
	DLLEXP void price_0(double &prix, double &ic);
	DLLEXP void price_t(double* prix, double* ic, double* past, int m, int n, double t);
	DLLEXP void delta_t(double* delta, double* past, int m, int n, double t);
	DLLEXP void hedge_T(int rebal, int n, double* portfolio, double* PnL, double *px, double*delta);
	DLLEXP void performance(int rebal, double* portfolio, double* PnL, int nb_simul);
	DLLEXP void monteCarloInstance(int size, double r, double* rho, double*sigma, double*spot, double*trend, double h, int samples, double T, int TimeSteps, char*optionType, double InvestissementInit);
	DLLEXP void monteCarloDestruct();
}